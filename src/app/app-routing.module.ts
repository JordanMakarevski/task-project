import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { BreakingBadComponent } from './components/breaking-bad/breaking-bad.component';
import { CreatePersonComponent } from './components/create-person/create-person.component';
import { CreateTaskComponent } from './components/create-task/create-task.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { PersonTaskComponent } from './components/person-task/person-task.component';
import { PersonComponent } from './components/person/person.component';
import { UserPostsComponent } from './components/user-posts/user-posts.component';
import { CanDeactivateGuard } from './guards/can-deactivate.guard';
const routes: Routes = [
  {
    path: 'person',
    component: PersonTaskComponent,
    children: [
      {
        path: ':id',
        component: PersonComponent,
      },
    ],
  },
  {
    path: 'create-person',
    canDeactivate: [CanDeactivateGuard],
    component: CreatePersonComponent,
  },
  {
    path: 'user-posts',
    component: UserPostsComponent,
  },
  {
    path: 'create-task/:id',
    canDeactivate: [CanDeactivateGuard],
    component: CreateTaskComponent,
  },
  {
    path: 'breaking-bad',
    component: BreakingBadComponent
  },
  {
    path: '',
    redirectTo: 'person',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: ErrorPageComponent,
  },
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
