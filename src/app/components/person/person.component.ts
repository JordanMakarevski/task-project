import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Router } from '@angular/router';
import { Person } from '../../interfaces/person';
import { PersonService } from 'src/app/services/person.service';
import { HttpClientService } from 'src/app/services/http-client.service';
import { Post } from 'src/app/interfaces/post';
@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss'],
})
export class PersonComponent implements OnInit {
  person: Person;
  constructor(
    private activatedRoute: ActivatedRoute,
    private personService: PersonService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      const id = Number(paramMap.get('id'));
      this.person = this.getPerson(id);
    });
  }

  getPerson(id: number): Person {
    return this.personService.getPersonById(id);
  }

  displayPersonTasks(person: Person) {
    this.personService.showTasks(person);
  }
  showNumberOfRemainingTasks(person: Person): number {
    return this.personService.notDoneTasks(person);
  }
  navigateToCreateTask(id: number) {
    this.router.navigate(['/create-task', id]);
  }
}
