import { Component, OnInit } from '@angular/core';
import { HttpClientService } from 'src/app/services/http-client.service';
import { Post } from 'src/app/interfaces/post';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';
@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.scss'],
})
export class UserPostsComponent implements OnInit {
  constructor(
    private httpClientService: HttpClientService,
    private router: Router
  ) {}
  post: Post[] = [];
  idControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.max(10),
    Validators.min(1),
  ]);

  ngOnInit(): void {}
  displayUserPosts(): void {
    this.httpClientService
      .getUsers(
        'https://jsonplaceholder.typicode.com/posts',
        this.idControl.value
      )
      .subscribe((posts: Post[]) => {
        this.post = posts;
      });
  }
}
