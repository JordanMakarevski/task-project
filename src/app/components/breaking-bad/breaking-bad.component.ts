import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BreakingBadService } from 'src/app/services/breaking-bad.service';
import { BreakingBadCharacters } from 'src/app/interfaces/breaking-bad-characters';
import {
  debounceTime,
  distinctUntilChanged,
  Observable,
  startWith,
} from 'rxjs';
import { switchMap } from 'rxjs';
@Component({
  selector: 'app-breaking-bad',
  templateUrl: './breaking-bad.component.html',
  styleUrls: ['./breaking-bad.component.scss'],
})
export class BreakingBadComponent implements OnInit {
  characters$?: Observable<BreakingBadCharacters[]>;
  charactersFormGroup: FormGroup = new FormGroup({
    characterName: new FormControl(''),
  });
  constructor(private breakingBadService: BreakingBadService) {}

  ngOnInit(): void {
    const characterNameControl: FormControl = this.charactersFormGroup.get(
      'characterName'
    ) as FormControl;
    this.characters$ = characterNameControl.valueChanges.pipe(
      debounceTime(600),
      distinctUntilChanged(),
      startWith(''),
      switchMap((result: string) =>
        this.breakingBadService.getCharacters(result)
      )
    );
  }
}
