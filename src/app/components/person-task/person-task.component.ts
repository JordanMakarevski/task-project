import { Component, OnInit } from '@angular/core';
import { PersonService } from 'src/app/services/person.service';
import { Person } from 'src/app/interfaces/person';
import { Task } from 'src/app/interfaces/task';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ParamMap } from '@angular/router';
@Component({
  selector: 'app-person-task',
  templateUrl: './person-task.component.html',
  styleUrls: ['./person-task.component.scss'],
})
export class PersonTaskComponent implements OnInit {
  person: Person[] = [];
  tasks: Task[] = [];
  color: string;
  constructor(
    private router: Router,
    private personService: PersonService,
    private activatedRoute: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.person = this.personService.getPerson();
    this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
      this.color = paramMap.get('color');
    });
  }
  navigateToCreatePersonComponent() {
    this.router.navigate(['/create-person']);
  }
  navigateToUserPostsComponent() {
    this.router.navigate(['/user-posts']);
  }
}
