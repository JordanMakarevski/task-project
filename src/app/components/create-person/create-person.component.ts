import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PersonService } from 'src/app/services/person.service';
import { Router } from '@angular/router';
import { DataSaved } from 'src/app/interfaces/data-saved';
@Component({
  selector: 'app-create-person',
  templateUrl: './create-person.component.html',
  styleUrls: ['./create-person.component.scss'],
})
export class CreatePersonComponent implements OnInit, DataSaved {
  personCreationForm: FormGroup = new FormGroup({
    nameControl: new FormControl('', [Validators.required]),
    ageControl: new FormControl('', [Validators.required]),
  });
  constructor(private router: Router, private personService: PersonService) {}
  submitPersonForm() {
    const name = this.personCreationForm.get('nameControl')?.value;
    const age = Number(this.personCreationForm.get('ageControl')?.value);
    if (this.personCreationForm.valid) {
      this.personService.createPerson(name, age);
      this.router.navigate(['person']);
    }
  }
  isDataSaved(): boolean {
    return !this.personCreationForm.dirty;
  }
  ngOnInit(): void {}
}
