import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/interfaces/task';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Person } from 'src/app/interfaces/person';
import { PersonService } from 'src/app/services/person.service';
import { Validators } from '@angular/forms';
import { DataSaved } from 'src/app/interfaces/data-saved';
@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss'],
})
export class CreateTaskComponent implements OnInit, DataSaved {
  taskCreationForm: FormGroup = new FormGroup({
    titleControl: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    doneControl: new FormControl('', [
      Validators.required,
      Validators.pattern('^(?:tru|fals)e$'),
    ]),
  });
  private person: Person;
  id: number;
  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      this.id = Number(paramMap.get('id'));
      this.person = this.personService.getPersonById(this.id);
    });
  }
  constructor(
    private personService: PersonService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  submitTaskForm() {
    const taskTitle = this.taskCreationForm.get('titleControl')?.value;
    const taskDone = this.taskCreationForm.get('doneControl')?.value;
    if (this.taskCreationForm.valid) {
      let done: boolean;
      if (taskDone === 'true') done = true;
      else done = false;
      const tasks: Task = {
        title: taskTitle,
        done: done,
      };
      this.personService.createTaskToPerson(this.person.id, tasks);
      this.router.navigate(['/person', this.person.id]);
    }
  }
  isDataSaved(): boolean {
    return !this.taskCreationForm.dirty;
  }
}
