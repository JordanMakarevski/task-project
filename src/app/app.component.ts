import { Component } from '@angular/core';
import { TaskService } from './services/task.service';
import { PersonService } from './services/person.service';
import { BreakingBadService } from './services/breaking-bad.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [TaskService, PersonService, BreakingBadService],
})
export class AppComponent {}
