import { Task } from './task';
export interface Person {
  id: number;
  name: string;
  age: number;
  tasks: Task[];
  showTasks: boolean;
}
