export interface DataSaved {
  isDataSaved(): boolean;
}
