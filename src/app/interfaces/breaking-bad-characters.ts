export interface BreakingBadCharacters {
  name: string;
  portrayed: string;
  status: string;
  nickname: string;
  img: string;
}
