import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs';
import { BreakingBadCharacters } from '../interfaces/breaking-bad-characters';
@Injectable({
  providedIn: 'root',
})
export class BreakingBadService {
  constructor(private httpClient: HttpClient) {}

  getCharacters(name: string): Observable<any> {
    const options = {
      params: {
        name: name,
      },
    };
    return this.httpClient.get(environment.breakingBadApi, options).pipe(
      map((result: any) => {
        return result.map((item: any) => {
          return {
            name: item.name,
            portrayed: item.portrayed,
            status: item.status,
            nickname: item.nickname,
            img: item.img,
          };
        });
      })
    );
  }
}
