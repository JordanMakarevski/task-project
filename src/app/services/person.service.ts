import { Injectable, OnInit } from '@angular/core';
import { TaskService } from './task.service';
import { Task } from '../interfaces/task';
import { Person } from '../interfaces/person';
import { ActivatedRoute } from '@angular/router';
@Injectable()
export class PersonService {
  tasks: Task[] = this.taskService.getTasks();
  person: Person[] = [
    {
      id: 0,
      name: 'John',
      age: 53,
      tasks: [this.tasks[5], this.tasks[2], this.tasks[1]],
      showTasks: false,
    },
    {
      id: 1,
      name: 'Andrew',
      age: 23,
      tasks: [this.tasks[3], this.tasks[0], this.tasks[6]],
      showTasks: false,
    },
  ];
  constructor(
    private taskService: TaskService,
    private activatedRoute: ActivatedRoute
  ) {}
  getPerson(): Person[] {
    return this.person;
  }
  getPersonById(id: number): Person {
    return this.person.find((person) => person.id === id)!;
  }

  showTasks(person: Person): void {
    person.showTasks = !person.showTasks;
  }
  notDoneTasks(person: Person): number {
    let numberOfTasks = person.tasks.length;
    let numberOfTasksLeft = person.tasks.filter(function (task) {
      return task.done;
    });
    return numberOfTasks - numberOfTasksLeft.length;
  }
  createPerson(name: string, age: number): void {
    const personID: number = this.person.length;
    const tasks: Task[] = [];
    const prs: Person = {
      id: personID,
      tasks: tasks,
      name: name,
      age: age,
      showTasks: false,
    };
    this.person.push(prs);
  }
  createTaskToPerson(id: number, task: Task) {
    let person: Person = this.getPersonById(id);
    person.tasks.push(task);
  }
}
