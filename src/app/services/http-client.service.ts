import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Person } from '../interfaces/person';
import { Post } from '../interfaces/post';
@Injectable({
  providedIn: 'root',
})
export class HttpClientService {
  constructor(private httpClient: HttpClient) {}
  url = 'https://jsonplaceholder.typicode.com/posts';
  getUsers(url: string, userId: string): Observable<Post[]> {
    return this.httpClient.get<Post[]>(`${url}?userId=${userId}`);
  }
  createUser(url: string, person: Person): Observable<Post> {
    return this.httpClient.post<Post>(url, person);
  }
  deleteUser(url: string, id: number): Observable<void> {
    return this.httpClient.delete<void>(`${url}/${id}`);
  }
  updateUser(url: string, id: number, post: Post): Observable<Post> {
    return this.httpClient.put<Post>(`${url}/${id}`, post);
  }
}
