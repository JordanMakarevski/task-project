import { Injectable } from '@angular/core';
import { Task } from '../interfaces/task';
@Injectable()
export class TaskService {
  tasks: Task[] = [
    {
      title: 'Eat',
      done: false,
    },
    {
      title: 'Go to work',
      done: true,
    },
    {
      title: 'Workout',
      done: false,
    },
    {
      title: 'Sleep',
      done: true,
    },
    {
      title: 'Change tires',
      done: false,
    },
    {
      title: 'Do Laundry',
      done: true,
    },
    {
      title: 'Buy a new computer',
      done: true,
    },
  ];
  constructor() {}
  getTasks(): Task[] {
    return this.tasks;
  }
}
