import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanDeactivate,
  Data,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { CreatePersonComponent } from '../components/create-person/create-person.component';
import { DataSaved } from '../interfaces/data-saved';
@Injectable({
  providedIn: 'root',
})
export class CanDeactivateGuard implements CanDeactivate<DataSaved> {
  canDeactivate(
    component: DataSaved,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!component.isDataSaved()) {
      if (confirm('Are you sure?')) {
        return true;
      } else return false;
    }
    return true;
  }
}
