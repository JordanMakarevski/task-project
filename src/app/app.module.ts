import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { PersonTaskComponent } from './components/person-task/person-task.component';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PersonComponent } from './components/person/person.component';
import { TaskComponent } from './components/task/task.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreatePersonComponent } from './components/create-person/create-person.component';
import { CreateTaskComponent } from './components/create-task/create-task.component';
import { UserPostsComponent } from './components/user-posts/user-posts.component';
import { BreakingBadComponent } from './components/breaking-bad/breaking-bad.component';
@NgModule({
  declarations: [
    PersonComponent,
    AppComponent,
    TaskComponent,
    PersonTaskComponent,
    ErrorPageComponent,
    CreatePersonComponent,
    CreateTaskComponent,
    UserPostsComponent,
    BreakingBadComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
